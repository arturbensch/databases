from datetime import datetime
from glob import glob
import os
import re
import requests as req

#Função para encontrar o ultimo arquivo baixado
def find_last_period(output_folder):
    files = glob(output_folder + '/*.csv')
    periods = []

    regex = r'(\d\d\d\d-\d)'

    if(len(files) == 0):
      return None

    for file in files:
      match = re.search(regex, file)
      
      if match:
        [year, semester] = file[slice(match.start(), match.end())].split('-')
        periods.append({ 'year': int(year),'semester': int(semester) })

    sorted_periods = sorted(periods, key = lambda period: (period['year'], period['semester']), reverse=True)

    return sorted_periods[0]


#Função para pegar todos os semestre e ano entre o ultimo arquivo baixado e a data atual
def check_for_updates(last_period):
  current_date = datetime.now()
  current_year = current_date.year
  current_semester = 1 if current_date.month < 7 else 2

  periods = []

  if(last_period['year'] == current_year) and (last_period['semester'] == current_semester):
    return periods

  if (last_period['semester'] < current_semester):
    period = { 'year': current_year, 'semester': 2 }
    periods.append(period)

  elif last_period['year'] < current_year:

    if(last_period['semester'] < 2):
      period = { 'year': last_period['year'], 'semester': 2 }
      periods.append(period)

    for year in range(last_period['year'] + 1, current_year + 1):
      period_1 = { 'year': year, 'semester': 1 }
      periods.append(period_1)

      period_2 = { 'year': year, 'semester': 2 }
      periods.append(period_2)

  if(current_semester == 1): 
    periods.pop()

  return periods

#Função que prepara a requisição(request) que será feita para o site para baixar os arquivos
def mount_requests(periods, download_address, output_folder):
  requests = []

  for period in periods:
      year = period['year']
      semester = period['semester']

      url = download_address.format(year=year, semester=semester)
      output_file = output_folder + '/lista_de_espera_{year}-{semester}.csv'.format(year=year, semester=semester)

      requests.append({ 'url': url, 'output_file': output_file })
  
  return requests

#Função de download
def download_files(requests):

  for request in requests:
    print(request['url'])
    response = req.get(request['url'], verify=False, allow_redirects=True)

    if(response.status_code == 200):
      file = open(request['output_file'], 'wb')
      file.write(response.content)
      file.close()

#Main
if __name__ == '__main__':
  download_address = 'https://dadosabertos.mec.gov.br/images/conteudo/sisu/{year}/ListagemListaEspera_{year}-{semester}.csv'
  # download_address = 'https://dadosabertos.mec.gov.br/images/conteudo/sisu/{year}/lista_de_espera_sisu_{year}_{semester}.csv'
  
  output_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../../data/sisu/lista_de_espera/raw')

  last_period = find_last_period(output_folder)

  if(last_period == None):
    periods = [
      { 'year': 2020, 'semester': 2 },
      { 'year': 2021, 'semester': 1 }
    ]

  else:
    periods = check_for_updates(last_period)

  requests = mount_requests(periods, download_address, output_folder)

  download_files(requests)