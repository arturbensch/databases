#!/usr/bin/env python
# coding: utf-8

# In[4]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import seaborn as sns; sns.set()
from datetime import datetime
import calendar
import os
from os import listdir
from os.path import isfile, join

data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/esus/')

# ## Lê dados processados do eSUS-VE

# In[5]:


df_orig = pd.read_csv(data_dir + 'todos_registros_esus-ve.csv')


# In[12]:


# retira positivos descartados (considera preenchimento errado)
df_orig = df_orig[~((df_orig['classificacaoFinal'] == 'Descartado') & (df_orig['resultadoTeste'] == 'Positivo'))]

# retira registros sem informação sobre conclusão
df_orig = df_orig[~((df_orig['classificacaoFinal'].isna()) & (df_orig['resultadoTeste'].isna()))]

# so pra ficar mais bonitinho
df_orig['classificacaoFinal'] = df_orig['classificacaoFinal'].fillna("Não preenchido")

confirmados = ['Confirmado Laboratorial',
       'Confirmado Clínico-Epidemiológico',
       'Confirmado Clínico-Imagem',
       'Confirmação Laboratorial', 'Confirmado por Critério Clínico',
       'Confirmação Clínico Epidemiológico',
       'Confirmado Clinico-Imagem',
       'Confirmado Clinico-Epidemiologico']

# descartados são considerados descartados
df_orig.loc[(df_orig['classificacaoFinal'] == 'Descartado'), 'Classificação'] = 'Descartado'

# negativos são considerados descartados (por enquanto)
df_orig.loc[(df_orig['resultadoTeste'] == 'Negativo'), 'Classificação'] = 'Descartado'

# negativos são considerados descartados (por enquanto)
df_orig.loc[(df_orig['classificacaoFinal'] == 'Não preenchido') & (df_orig['resultadoTeste'] == 'Negativo'), 'Classificação'] = 'Descartado'


# confirmados ou positivos sao considerados confirmados
df_orig.loc[(df_orig['classificacaoFinal'].isin(confirmados)) | (df_orig['resultadoTeste'] == 'Positivo'), 'Classificação'] = 'Confirmado'

df_orig = df_orig[df_orig['Classificação'] == 'Confirmado']

df_orig.drop('Classificação', axis=1).to_csv(data_dir + 'registros_confirmados_esus-ve.csv', index=False)


# In[ ]:




