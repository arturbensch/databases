#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import seaborn as sns; sns.set()
from datetime import datetime
import calendar
import os
# from pathlib import Path
from os import listdir
from os.path import isfile, join

pd.set_option('display.max_rows', 6000)
pd.set_option('display.max_columns', 100)
pd.set_option('display.width', 1000)

data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/esus/')
data_mun = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/municipios/')


# ## Lê dados processados do eSUS-VE

# In[2]:


# df_orig = pd.read_csv(data_dir + 'todos_registros_esus-ve.csv')
df_orig = pd.read_csv(data_dir + 'registros_capitais_esus-ve.csv', parse_dates=['dataInicioSintomas'])



# Limpa idades inválidas
df_orig.loc[df_orig['idade'] > 110, 'idade'] = np.nan

# retira positivos descartados (considera preenchimento errado)
df_orig = df_orig[~((df_orig['classificacaoFinal'] == 'Descartado') & (df_orig['resultadoTeste'] == 'Positivo'))]

# retira registros sem informação sobre conclusão
df_orig = df_orig[~((df_orig['classificacaoFinal'].isna()) & (df_orig['resultadoTeste'].isna()))]

# so pra ficar mais bonitinho
df_orig['classificacaoFinal'] = df_orig['classificacaoFinal'].fillna("Não preenchido")

confirmados = ['Confirmado Laboratorial',
       'Confirmado Clínico-Epidemiológico',
       'Confirmado Clínico-Imagem',
       'Confirmação Laboratorial', 'Confirmado por Critério Clínico',
       'Confirmação Clínico Epidemiológico',
       'Confirmado Clinico-Imagem',
       'Confirmado Clinico-Epidemiologico']

# descartados são considerados descartados
df_orig.loc[(df_orig['classificacaoFinal'] == 'Descartado'), 'Classificação'] = 'Descartado'

# negativos são considerados descartados (por enquanto)
df_orig.loc[(df_orig['resultadoTeste'] == 'Negativo'), 'Classificação'] = 'Descartado'

# negativos são considerados descartados (por enquanto)
df_orig.loc[(df_orig['classificacaoFinal'] == 'Não preenchido') & (df_orig['resultadoTeste'] == 'Negativo'), 'Classificação'] = 'Descartado'


# confirmados ou positivos sao considerados confirmados
df_orig.loc[(df_orig['classificacaoFinal'].isin(confirmados)) | (df_orig['resultadoTeste'] == 'Positivo'), 'Classificação'] = 'Confirmado'

df_orig = df_orig[~df_orig['Classificação'].isna()]


df_orig['Grupo'] = pd.cut(df_orig['idade'], [0, 20, 40, 60, 110])
#df_orig['Grupo'] = pd.cut(df_orig['idade'], [0, 12, 18, 25, 55, 110])

df_pop = pd.read_csv(data_mun + 'populacao_nome_codigo.csv').rename(columns={'city_ibge_code': 'municipioIBGE'}).set_index('municipioIBGE')



df_total = df_orig.groupby(['municipioIBGE', 'dataInicioSintomas', 'Grupo'])[['Classificação']].count().rename(columns={'Classificação':'Total'})

df_total = df_total.unstack()


# In[21]:


df_positivos = df_orig[df_orig['Classificação'] == 'Confirmado']



# In[22]:


df_total_positivos = df_positivos.groupby(['municipioIBGE', 'dataInicioSintomas', 'Grupo'])[['Classificação']].count().rename(columns={'Classificação':'Positivos'})

df_total_positivos = df_total_positivos.unstack()


# In[23]:


df_total_pos = df_total.join(df_total_positivos)



# In[24]:


df_positividade = 100 * df_total_pos['Positivos'][:]/df_total_pos['Total'][:]



# In[25]:


df_positividade.columns = pd.MultiIndex.from_product([['Positividade'], df_positividade.columns])


# In[26]:


df_total_pos = df_total_pos.join(df_positividade)



# In[27]:


df_todos_pct = df_total_pos['Total'].rolling(7).mean().fillna(0).apply(lambda x: 100*x/x.sum(), axis=1)



# In[28]:


df_todos_pct = df_todos_pct.stack().reset_index().merge(df_pop.reset_index()).rename(columns={0:'Porcentagem'})



# In[30]:


df_positivos_pct = df_total_pos['Positivos'].rolling(7).mean().fillna(0).apply(lambda x: 100*x/x.sum(), axis=1)



# In[31]:


df_positivos_pct = df_positivos_pct.stack().reset_index().merge(df_pop.reset_index()).rename(columns={0:'Porcentagem'})



df_positivos_pct.drop('estimated_population_2019', axis=1).rename(columns={'Name':'municipio'}).to_csv(data_dir + 'percentual_positivos_faixa_etaria_esus-ve.csv', index=False)


# In[ ]:




