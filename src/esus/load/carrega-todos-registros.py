#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime
import calendar
import os
from os import listdir
from os.path import isfile, join


data_raw_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/esus/raw/')

data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../data/esus/')



# quais campos selecionar
# campos disponíveis: ['ÿid', 'dataNotificacao', 'dataInicioSintomas', 'dataNascimento', 'sintomas', 'profissionalSaude', 'cbo', 'condicoes', 'estadoTeste', 'dataTeste', 'tipoTeste', 'resultadoTeste', 'paisOrigem', 'sexo', 'estado', 'estadoIBGE', 'municipio', 'municipioIBGE', 'origem', 'cnes', 'estadoNotificacao', 'estadoNotificacaoIBGE', 'municipioNotificacao', 'municipioNotificacaoIBGE', 'excluido', 'validado', 'idade', 'dataEncerramento', 'evolucaoCaso', 'classificacaoFinal']
filtrar_campos = False
campos = ['municipioNotificacaoIBGE', 'dataNotificacao', 'dataInicioSintomas', 'tipoTeste', 'idade', 'resultadoTeste', 'classificacaoFinal']

# fazer parsing dos campos de data?
datas_a_processar = ['dataNotificacao', 'dataInicioSintomas']

# remover datas antes de março/2020 e futuras?
remover_data_invalidas = False

# ordenação dos dados (precisa ser subconjunto da variável campos)
campos_ordenar = ['municipioNotificacaoIBGE', 'dataNotificacao']

sample = False
sample_size = 0.01 #fração a ser amostrada

# arquivo de log e estatísticas sobre o dataset gerado
file_name = 'todos_registros_esus-ve{}'.format('_sample' if sample else '')
report_file = open(data_dir + '{}.txt'.format(file_name), "w")


# ## Processa arquivos eSUS-VE


# obtém lista de arquivos
files = [f for f in listdir(data_raw_dir) if isfile(join(data_raw_dir, f))]
files.sort()


# obtém sufixo (data) dos arquivos mais recentes
suffix = files[-1].split('-')[2]

# seleciona arquivos mais recentes
last_files = [f for f in files if f.endswith(suffix)]

# dataframe para registro de linhas inválidas
df_invalid_lines = pd.DataFrame({'file':[], 'invalid':[], 'total':[]})

today = pd.to_datetime('today')

campos_disponiveis = []

# função que processa um dataframe com dados raw
def process(df_orig, file):
    global campos_disponiveis, df_invalid_lines
    print('Processing {}'.format(file))
    
    if not len(campos_disponiveis): 
        campos_disponiveis = df_orig.columns
        print('Campos desponíveis: {}', campos_disponiveis)
        print('Campos selecionados: {}', campos if filtrar_campos else campos_disponiveis)
        
    #seleciona campos
    if filtrar_campos: df_orig = df_orig[campos]  
    
    
    # amostragem
    if sample: df_orig = df_orig.sample(frac=sample_size) 
        
    total = len(df_orig)
    
    df_orig.replace('undefined', np.nan, inplace=True)

    # faz parsing das datas
    # TODO: verificar as datas que não são reconhecidas e tentar tratar para que elas sejam aceitas
    for d in datas_a_processar:
        novo_campo = d #+ '_parsed'
        df_orig[novo_campo] = pd.to_datetime(df_orig[d], errors='coerce').dt.date
        # remove datas anteriores à pandemia e no futuro
        if remover_data_invalidas: df_orig = df_orig[(df_orig[novo_campo] >= pd.to_datetime('2020-03-01')) & (df_orig[novo_campo] <= today)]

    # df_orig['municipioIBGE'] = df_orig['municipioIBGE'].replace('undefined', None).astype(float)
    
    total_invalid = total - len(df_orig)
#     print("Invalid lines: {:,} of {:,}".format(total_invalid, total))
    df_invalid_lines = df_invalid_lines.append({'file':file, 'invalid':total_invalid, 'total':total}, ignore_index=True)

    return df_orig


# determina quais estados devem ser divididos em pedaços
# poderia ser simplificado fazendo o chunking de todos os estados, mas alguns arquivos causam erros diferentes em estratégias diferentes de leitura :/
chunkeables = ['_sp']

to_chunk = [f for f in last_files if any(e in f for e in chunkeables)]
not_to_chunk = [f for f in last_files if all(e not in f for e in chunkeables)]

print("to chunk\n", to_chunk, "\nnot to chunk\n", not_to_chunk, '\nthats the question')

df_orig = pd.DataFrame()
chunksize = 10 ** 5

# processa arquivos que precisam ser cortados
for file in to_chunk:
    df = pd.DataFrame()
    
    for chunk in pd.read_csv(data_raw_dir + file, sep=';', encoding='latin-1',engine='python', error_bad_lines=False, chunksize=chunksize):
        chunk = process(chunk, file)
        df = pd.concat([df, chunk])

    df = df.sort_values(campos_ordenar)
    df_orig = pd.concat([df_orig, df])

print(df_orig.dtypes)

# processa arquivos que não precisam ser cortados
for file in not_to_chunk:
    print(file)
    df = pd.DataFrame()
    
    df = pd.read_csv(data_raw_dir + file, sep=';', encoding='latin-1',engine='python', error_bad_lines=False)
    df = process(df, file)
    df = df.sort_values(campos_ordenar)
    
    df_orig = pd.concat([df_orig, df])


print(df_orig.dtypes)

df_invalid_lines = df_invalid_lines.groupby('file').sum()

df_invalid_lines['pct_invalid'] = 100 * df_invalid_lines['invalid']/df_invalid_lines['total']





report_file.write("Registros inválidos:\n")
report_file.write(str(df_invalid_lines))

# TODO: carregar registros no postgresql

report_file.close()


# In[ ]:




