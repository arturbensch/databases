#!/bin/bash
pipenv run python src/esus/transform/processa-todos-registros.py
pipenv run python src/esus/transform/filtra_confirmados.py
pipenv run python src/esus/transform/filtra_maiores_cidades.py
pipenv run python src/esus/transform/calcula_taxas_testagem_capitais_e_maiores_cidades.py
pipenv run python src/esus/transform/positivos_faixa_etaria_capitais.py