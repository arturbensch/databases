# Databases

Código de processamento de dados do Ciência de Dados por uma Causa


### Prerequisites

- [Python 3](https://www.python.org/downloads/).
- [Pipenv](https://github.com/pypa/pipenv) to manage or dependencies.

### Installing

To run locally:

Install pipenv

```
pip3 install pipenv
```

Activate virtual environment

```
python3 -m pipenv shell
```

Install the dependencies

```
pipenv install
```

Run all scripts following the examples below:

```
sh run_extract.sh <dataset>
sh run_transform.sh <dataset>
sh run_load.sh <dataset>
```

 The `<dataset>` parameter can be:
 - esus (esus Covid-19 data)
 - sisu_regular
 - sisu_espera
 - censo


